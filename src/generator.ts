import { invariant } from './utils'

interface Options {
  restVelocityThreshold?: number
  timeStep?: number
}

interface Spring {
  mass: number
  damping: number
  stiffness: number
}

/**
 * Implements a spring physics simulation based on the equations behind
 * damped harmonic oscillators (https://en.wikipedia.org/wiki/Harmonic_oscillator#Damped_harmonic_oscillator).
 *
 * Returns an iterator that yields the position of the spring over time.
 * The iterator completes when the spring's velocity
 * is lower than `options.restVelocityThreshold`,
 * which defaults to `0.001`.
 * By default, the simulation runs with a time step
 * of `16.667` i.e. at 60 frames per second.
 * This can be configured with `options.timeStep`.
 */
function* springGenerator(
  from: number,
  to: number,
  initialVelocity: number,
  spring: Spring,
  options: Options = {},
) {
  const { damping, mass, stiffness } = spring
  const { restVelocityThreshold = 0.001, timeStep = 16.667 } = options
  let springTime = timeStep
  let velocity = initialVelocity

  invariant(mass > 0, 'Mass value must be greater than 0')
  invariant(stiffness > 0, 'Stiffness value must be greater than 0')

  function advanceSpring() {
    const c = damping
    const m = mass
    const k = stiffness
    const fromValue = from
    const toValue = to
    const v0 = -initialVelocity

    let zeta = c / (2 * Math.sqrt(k * m)) // damping ratio (dimensionless)
    const omega0 = Math.sqrt(k / m) / 1000 // undamped angular frequency of the oscillator (rad/ms)
    const omega1 = omega0 * Math.sqrt(1.0 - zeta * zeta) // exponential decay
    const omega2 = omega0 * Math.sqrt(zeta * zeta - 1.0) // frequency of damped oscillation
    const x0 = toValue - fromValue // initial displacement of the spring at t = 0

    zeta = Math.min(zeta, 1)

    let oscillation = 0.0
    const t = springTime
    if (zeta < 1) {
      // Under damped
      const envelope = Math.exp(-zeta * omega0 * t)
      oscillation =
        toValue -
        envelope *
          (((v0 + zeta * omega0 * x0) / omega1) * Math.sin(omega1 * t) +
            x0 * Math.cos(omega1 * t))
      // This looks crazy -- it's actually just the derivative of the
      // oscillation function
      velocity =
        zeta *
          omega0 *
          envelope *
          ((Math.sin(omega1 * t) * (v0 + zeta * omega0 * x0)) / omega1 +
            x0 * Math.cos(omega1 * t)) -
        envelope *
          (Math.cos(omega1 * t) * (v0 + zeta * omega0 * x0) -
            omega1 * x0 * Math.sin(omega1 * t))
    } else if (zeta === 1) {
      // Critically damped
      const envelope = Math.exp(-omega0 * t)
      oscillation = toValue - envelope * (x0 + (v0 + omega0 * x0) * t)
      velocity = envelope * (v0 * (t * omega0 - 1) + t * x0 * (omega0 * omega0))
    } else {
      // Overdamped
      const envelope = Math.exp(-zeta * omega0 * t)
      oscillation =
        toValue -
        (envelope *
          ((v0 + zeta * omega0 * x0) * Math.sinh(omega2 * t) +
            omega2 * x0 * Math.cosh(omega2 * t))) /
          omega2
      velocity =
        (envelope *
          zeta *
          omega0 *
          (Math.sinh(omega2 * t) * (v0 + zeta * omega0 * x0) +
            x0 * omega2 * Math.cosh(omega2 * t))) /
          omega2 -
        (envelope *
          (omega2 * Math.cosh(omega2 * t) * (v0 + zeta * omega0 * x0) +
            omega2 * omega2 * x0 * Math.sinh(omega2 * t))) /
          omega2
    }

    springTime += timeStep
    return oscillation
  }

  yield advanceSpring()
  while (Math.abs(velocity) > restVelocityThreshold) {
    yield advanceSpring()
  }
  return advanceSpring()
}

export default springGenerator
